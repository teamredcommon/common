﻿using UnityEngine;
using System.Collections;

public static class CSVReader  {


    public static string[] ReadCsv(string filePath, System.StringSplitOptions splitOptions)
    {
        TextAsset rawTextData = Resources.Load(filePath) as TextAsset;

        string[] csvData = rawTextData.text.Split(new char[] { '\n', '\r', ','}, splitOptions);



        return csvData;
    }
}
